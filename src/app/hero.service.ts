import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Hero } from './hero';
import { Heroes } from './default-heroes';

const STORAGE_KEY = 'tour_of_heroes';

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  heroes: Hero[] = [];

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {
    this.heroes = this.storage.get(STORAGE_KEY) || Heroes;
  }

  public addHero(name: string): void {
    let id;
    if (this.heroes.length) {
      id = this.heroes[this.heroes.length - 1].id + 1;
    } else {
      id = 1;
    }
    this.heroes.push({ id: id, name: name });
    this.updateStorage();
  }

  public getHeroes(): Hero[] {
    return this.heroes;
  }

  public getHero(id: number): Hero {
    return this.heroes.find(hero => hero.id === id);
  }

  public deleteHero(id: number): void {
    this.heroes = this.heroes.filter(hero => hero.id !== id);
    this.updateStorage();
  }

  public updateHero(id: number, edit: Hero): void {
    const index = this.heroes.findIndex(hero => hero.id === id);
    if (index !== -1 && id === edit.id) {
      this.heroes[index] = edit;
      this.updateStorage();
    }
  }

  private updateStorage(): void {
    this.storage.set(STORAGE_KEY, this.heroes);
  }

  public resetHeroes(): void {
    this.heroes = Heroes;
  }
}
