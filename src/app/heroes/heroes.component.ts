import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  heroes: Hero[];
  newName: '';

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroes = this.heroService.getHeroes();
  }

  deleteHero(id: number): void {
    this.heroService.deleteHero(id);
    this.heroes = this.heroes.filter(hero => hero.id !== id);
  }

  addHero(): void {
    if (this.newName) {
      this.heroService.addHero(this.newName);
      this.newName = '';
    }
  }

  resetHeroes(): void {
    this.heroService.resetHeroes();
    this.getHeroes();
  }
}
