import { Component, OnInit, Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HeroService } from '../hero.service';
import { Hero } from '../hero';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {
  @Input() hero: Hero;
  edit: Hero;
  newName = '';

  constructor(private route: ActivatedRoute,
              private heroService: HeroService,
              private location: Location) { }

  ngOnInit() {
    this.getHero();
  }

  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.hero = this.heroService.getHero(id);
    if (this.hero) {
      this.edit = { ...this.hero };
    } else {
      this.goBack();
    }
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    // this.hero.name = this.newName;
    this.heroService.updateHero(this.hero.id, this.edit);
    this.getHero();
  }

  delete(): void {
    this.heroService.deleteHero(this.hero.id);
    this.goBack();
  }
}
